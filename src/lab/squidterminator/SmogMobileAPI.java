package lab.squidterminator;

public interface SmogMobileAPI {
	/**
	 * Restful
	 */
	public static final String BASE = "http://smogmobile.appspot.com";
	public static final String CASE = BASE + "/case/";
	public static final String IMAGE = BASE + "/image/";
	public static final String VIDEO = BASE + "/video/";
	public static final String COMPLETE = BASE + "/case/complete/";

	public static interface Personal {
		public static final String NAME = "App_Name";
		public static final String ID = "App_id";
		public static final String MAINTEL = "App_Tel";
		public static final String OPT_CELLPHONE = "App_Tel3";
		public static final String ADDR_STATE = "App_Unit";
		public static final String ADDR_DISTRICT = "App_Coun";
		public static final String ADDR = "App_Add";
		public static final String REPLY = "IsReply";
		public static final String REWARD = "IsGetMoney";
		public static final String OPT_PAY = "PayType";
		public static final String OPT_EMAIL = "App_Email";
		public static final String OPT_FAX = "App_FAX";
		public static final String OPT_BANKNAME = "v_headbanks";
		public static final String OPT_BANKACCOUNT = "BankAccount";
	}

	public static interface CarFind {
		public static final String PLATE1 = "Car_No1";
		public static final String PLATE2 = "Car_No2";
		public static final String TYPE = "Car_Kind";
		public static final String COLOR1 = "App_CarColor1";
		public static final String COLOR2 = "App_CarColor2";
		public static final String COLOR3 = "App_CarColor3";
		public static final String ADDR_STATE = "Find_Unit";
		public static final String ADDR_DISTRICT = "Find_Coun";
		public static final String ADDR = "Find_Add";
		public static final String TIME = "datetime";
		public static final String FACT = "fact";
	}

	public static final String GAE_KEY = "key";
	public static final String IMAGE_KEY = "image";
	public static final String VIDEO_KEY = "video";
	public static final String ERROR_KEY = "error";

	public static interface Response {
		public static final String STATUS = "status";
		public static final String CASE_NUMBER = "case_number";
		public static final String IMAGES = "images";
		public static final String VIDEOS = "videos";
	}
}