package lab.squidterminator.svcpvd;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

public class SquidProvider extends ContentProvider {
	public static final String AUTHORITY = SquidProvider.class.getName()
			.toLowerCase();
	private static final UriMatcher URI_MATCHER = new UriMatcher(
			UriMatcher.NO_MATCH);
	private static final int SQUID = 0, SQUID_ID = 1;
	static {
		URI_MATCHER.addURI(AUTHORITY, Visitor.OfSquid.MATCH, SQUID);
		URI_MATCHER.addURI(AUTHORITY, Visitor.OfSquid.MATCH_ID + "#", SQUID_ID);
	}
	private static final String DB_NAME = "__db_squidterminator__";
	private static final int DB_VERSION = 8;

	public static interface Squid extends BaseColumns {

		/**
		 * The content:// style URI for this table
		 */
		public static final Uri CONTENT_URI = Uri.parse(Visitor.SCHEME
				+ AUTHORITY + Visitor.OfSquid.PATH);

		/**
		 * The content:// style URI for this table
		 */
		public static final Uri CONTENT_ITEM_URI = Uri.parse(Visitor.SCHEME
				+ AUTHORITY + Visitor.OfSquid.PATH_ID);

		public static final String VIDEO = "video";
		public static final String THUMBNAIL1 = "thumbnail1";
		public static final String THUMBNAIL2 = "thumbnail2";
		public static final String THUMBNAIL3 = "thumbnail3";

		public static final String LONGITUDE = "longitude";
		public static final String LATITUDE = "latitude";
		public static final String DATE = "report_time";
		public static final String STATE_INDEX = "state";
		public static final String DISTRICT_INDEX = "district";
		public static final String ADDRESS = "address";

		public static final String CAR_LICENSE1 = "car_license1";
		public static final String CAR_LICENSE2 = "car_license2";
		public static final String CAR_TYPE_INDEX = "car_type";

		public static final String GAE_KEY = "gae_key";
		public static final String STATUS = "status";
		public static final String GOV_ID = "gov_id";
	}

	/**
	 * Implementation defined!!
	 * @author TomChen
	 *
	 */
	private static abstract class Visitor {
		private static final String SCHEME = "content://";
		private static final String SLASH = "/";

		private final boolean mParseId;
		private final String mTableName, mGroupBy, mDefaultOrder;
		private final Uri mContentUri, mContentItemUri;

		protected Visitor(boolean parseId, String tableName, String groupBy,
				String defaultOrder, Uri contentUri, Uri contentItemUri) {
			super();
			this.mParseId = parseId;
			this.mTableName = tableName;
			this.mGroupBy = groupBy;
			this.mDefaultOrder = defaultOrder;
			this.mContentUri = contentUri;
			this.mContentItemUri = contentItemUri;
		}

		protected String buildSelection(Uri uri, String selection) {
			StringBuilder b = new StringBuilder(BaseColumns._ID);
			b.append('=');
			b.append(ContentUris.parseId(uri));
			if (selection != null) {
				b.append(" AND ");
				b.append(selection);
			}
			return b.toString();
		}

		protected abstract String createTable();

		private static final class OfSquid extends Visitor {
			private static final String MATCH = "squids/";
			private static final String MATCH_ID = "squid/";

			private static final String PATH = SLASH + MATCH;
			private static final String PATH_ID = SLASH + MATCH_ID;

			private static final String TABLE_NAME = "report_list";

			protected OfSquid(boolean parseId) {
				super(parseId, TABLE_NAME, null, Squid.DATE + " DESC",
						Squid.CONTENT_URI, Squid.CONTENT_ITEM_URI);
			}

			@Override
			protected String createTable() {
				StringBuilder b = new StringBuilder("CREATE TABLE ");
				b.append(TABLE_NAME).append(" (");
				b.append(Squid._ID).append(
						" INTEGER primary key autoincrement,");
				b.append(Squid.VIDEO).append(" TEXT,");
				b.append(Squid.THUMBNAIL1).append(" TEXT,");
				b.append(Squid.THUMBNAIL2).append(" TEXT,");
				b.append(Squid.THUMBNAIL3).append(" TEXT,");
				//
				b.append(Squid.LONGITUDE).append(" REAL,");
				b.append(Squid.LATITUDE).append(" REAL,");
				b.append(Squid.DATE).append(" INTEGER,");
				b.append(Squid.STATE_INDEX).append(" INTEGER,");
				b.append(Squid.DISTRICT_INDEX).append(" INTEGER,");
				b.append(Squid.ADDRESS).append(" TEXT,");
				//
				b.append(Squid.CAR_LICENSE1).append(" TEXT,");
				b.append(Squid.CAR_LICENSE2).append(" TEXT,");
				b.append(Squid.CAR_TYPE_INDEX).append(" TEXT,");
				//
				b.append(Squid.GAE_KEY).append(" TEXT,");
				b.append(Squid.STATUS).append(" TEXT,");
				b.append(Squid.GOV_ID).append(" TEXT");
				//
				b.append(");");
				return b.toString();
			}
		}
	}

	private final class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper() {
			super(getContext(), DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.beginTransaction();
			try {
				db.execSQL(new Visitor.OfSquid(false).createTable());
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Kills the table and existing data
			db.execSQL("DROP TABLE IF EXISTS " + Visitor.OfSquid.TABLE_NAME);
			// Recreates the database with a new version
			this.onCreate(db);
		}
	}

	private DatabaseHelper mOpenHelper;

	@Override
	public boolean onCreate() {
		this.mOpenHelper = new DatabaseHelper();
		return true;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	private final ContentResolver getContentResolver() {
		return this.getContext().getContentResolver();
	}

	private final void notifyChange(Uri uri) {
		if (uri != null)
			this.getContentResolver().notifyChange(uri, null);
	}

	private final Visitor match(Uri uri) {
		switch (URI_MATCHER.match(uri)) {
		case SQUID:
			return new Visitor.OfSquid(false);
		case SQUID_ID:
			return new Visitor.OfSquid(true);
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Visitor visitor = this.match(uri);
		if (TextUtils.isEmpty(sortOrder))
			sortOrder = visitor.mDefaultOrder;
		//
		if (visitor.mParseId)
			selection = visitor.buildSelection(uri, selection);
		Cursor c = this.mOpenHelper.getReadableDatabase().query(
				visitor.mTableName, projection, selection, selectionArgs,
				visitor.mGroupBy, null, sortOrder);
		//
		if (c != null)
			c.setNotificationUri(this.getContentResolver(), visitor.mContentUri);
		return c;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Visitor visitor = this.match(uri);
		long id = this.mOpenHelper.getWritableDatabase().insert(
				visitor.mTableName, null, values);
		if (id == -1)
			throw new SQLException("Failed to insert row into " + uri);
		try {
			return ContentUris.withAppendedId(visitor.mContentItemUri, id);
		} finally {
			this.notifyChange(visitor.mContentUri);
		}
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		Visitor visitor = this.match(uri);
		if (visitor.mParseId)
			selection = visitor.buildSelection(uri, selection);
		try {
			return this.mOpenHelper.getWritableDatabase().update(
					visitor.mTableName, values, selection, selectionArgs);
		} finally {
			this.notifyChange(visitor.mContentUri);
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		Visitor visitor = this.match(uri);
		if (visitor.mParseId)
			selection = visitor.buildSelection(uri, selection);
		try {
			return this.mOpenHelper.getWritableDatabase().delete(
					visitor.mTableName, selection, selectionArgs);
		} finally {
			this.notifyChange(visitor.mContentUri);
		}
	}
}