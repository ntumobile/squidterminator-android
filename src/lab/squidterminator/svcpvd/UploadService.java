package lab.squidterminator.svcpvd;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import lab.squidterminator.PersonalInfos;
import lab.squidterminator.R;
import lab.squidterminator.SmogMobileAPI;
import lab.squidterminator.SquidModel;
import lab.squidterminator.activity.NavigationBarActivity;
import lab.squidterminator.util.ThinMultipartEntity;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;

public class UploadService extends IntentService {

	private static final String TAG = UploadService.class.getSimpleName();

	private static final int NOTIFICATION_ID = 200;

	private NotificationManager mNotificationManager;

	public UploadService() {
		super("Uploader");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	}

	public void onDestroy() {
		super.onDestroy();
		mNotificationManager = null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		return START_STICKY;
	}

	private void sendNotification(int message) {
		PendingIntent pi = PendingIntent.getService(getApplicationContext(), 0,
				new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
		Notification notif = new Notification(R.drawable.launcher_icon, null,
				System.currentTimeMillis());
		notif.setLatestEventInfo(this, getString(R.string.app_name),
				getString(message), pi);
		notif.flags = Notification.FLAG_AUTO_CANCEL;
		mNotificationManager.notify(NOTIFICATION_ID, notif);
	}

	private SquidModel mModel;
	private HttpClient mHC;

	@Override
	protected void onHandleIntent(Intent intent) {
		long id = intent.getLongExtra(NavigationBarActivity.EXTRA_ID, -1L);
		if (id == -1L)
			return;
		this.sendNotification(R.string.notification_uploading);
		this.mModel = SquidModel.createInstance()
				.getById(getContentResolver(), id);
		Log.i(TAG, "Squid is " + this.mModel);
		//
		this.mHC = new DefaultHttpClient();
		try {
			this.doPosts();
		} catch (Exception e) {
			Log.w(TAG, e);
			/*this.startService(new Intent(this, UploadService.class).putExtra(
					NavigationBarActivity.EXTRA_ID, id));*/
		} finally {
			this.mHC.getConnectionManager().shutdown();
		}
	}

	private void doPosts() throws Exception {
		JSONObject json = this.toJSONObject(this.postCase());
		if (json.has(SmogMobileAPI.ERROR_KEY)) {
			this.sendNotification(R.string.notification_upload_problem);
			this.mModel.setStatus(json.getString(SmogMobileAPI.ERROR_KEY));
			this.mModel.save(getContentResolver());
			return;
		}
		this.mModel.setGAEKey(json.getString(SmogMobileAPI.GAE_KEY));
		this.mModel.setStatus(json.getString(SmogMobileAPI.Response.STATUS));
		this.mModel.save(getContentResolver());
		Log.d(TAG, this.mModel.getGAEKey());
		//
		for (int i = 0, size = 0; i < SquidModel.MEDIA_COUNT; ++i) {
			Uri imageUri = this.mModel.getImageUri(i);
			if (imageUri == null)
				continue;
			++size;
			json = this.toJSONObject(this.postImage(imageUri));
			if (json.has(SmogMobileAPI.ERROR_KEY)) {
				this.sendNotification(R.string.notification_upload_problem);
				this.mModel.setStatus(json.getString(SmogMobileAPI.ERROR_KEY));
				this.mModel.save(getContentResolver());
				return;
			}
			JSONArray ids = json.getJSONArray(SmogMobileAPI.Response.IMAGES);
			if (ids.length() != size)
				throw new RuntimeException("images size not match");
		}
		if (this.mModel.getVideoUri() != null) {
			json = this.toJSONObject(this.postVideo());
			if (json.has(SmogMobileAPI.ERROR_KEY)) {
				this.sendNotification(R.string.notification_upload_problem);
				this.mModel.setStatus(json.getString(SmogMobileAPI.ERROR_KEY));
				this.mModel.save(getContentResolver());
				return;
			}
		}
		json = this.toJSONObject(this.postComplete());
		this.mModel.setStatus(json.getString(SmogMobileAPI.Response.STATUS));
		this.mModel.save(getContentResolver());
		this.sendNotification(R.string.notification_upload_finish);
	}

	private HttpPost postCase() throws IOException {
		Resources res = this.getResources();
		ArrayList<NameValuePair> list = this.mModel.fillForm(res);
		PersonalInfos personal = new PersonalInfos(this);
		list.addAll(personal.fillForm(res));
		personal.close();
		//
		HttpPost post = new HttpPost(SmogMobileAPI.CASE);
		post.setEntity(new UrlEncodedFormEntity(list));
		return post;
	}

	private HttpPost postImage(Uri imageUri) throws IOException {
		ThinMultipartEntity entity = new ThinMultipartEntity();
		entity.addPart(SmogMobileAPI.GAE_KEY, this.mModel.getGAEKey());
		InputStream is = null;
		try {
			is = this.getContentResolver().openInputStream(imageUri);
			entity.addPart(SmogMobileAPI.IMAGE_KEY,
					imageUri.getLastPathSegment(), is);
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
				}
		}
		HttpPost post = new HttpPost(SmogMobileAPI.IMAGE);
		post.setEntity(entity);
		return post;
	}

	private HttpPost postVideo() throws IOException {
		ThinMultipartEntity entity = new ThinMultipartEntity();
		entity.addPart(SmogMobileAPI.VIDEO_KEY, this.mModel.getGAEKey());
		InputStream is = null;
		try {
			is = this.getContentResolver().openInputStream(
					this.mModel.getVideoUri());
			entity.addPart(SmogMobileAPI.VIDEO_KEY, is.toString(), is);
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e1) {
				}
		}
		HttpPost post = new HttpPost(SmogMobileAPI.VIDEO);
		post.setEntity(entity);
		return post;
	}

	private HttpPost postComplete() throws IOException {
		ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair(SmogMobileAPI.GAE_KEY, this.mModel
				.getGAEKey()));
		HttpPost post = new HttpPost(SmogMobileAPI.COMPLETE);
		post.setEntity(new UrlEncodedFormEntity(list));
		return post;
	}

	private JSONObject toJSONObject(HttpPost post)
			throws ClientProtocolException, IOException, ParseException,
			JSONException {
		HttpResponse hr = this.mHC.execute(post);
		return new JSONObject(EntityUtils.toString(hr.getEntity()));
	}
}
