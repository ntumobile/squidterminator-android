package lab.squidterminator.svcpvd;

import java.io.IOException;
import java.util.ArrayList;

import lab.squidterminator.SmogMobileAPI;
import lab.squidterminator.svcpvd.SquidProvider.Squid;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;


public class GovIdFetchService extends IntentService {

	public GovIdFetchService() {
		super("GovId Fetcher");
	}

	private static final String[] sProjection = new String[] { Squid._ID,
			Squid.GAE_KEY, };

	private ArrayList<ContentProviderOperation> mList;
	private HttpClient mHC;

	@Override
	protected void onHandleIntent(Intent intent) {
		ContentResolver cr = this.getContentResolver();
		Cursor c = cr.query(Squid.CONTENT_URI, sProjection, Squid.GAE_KEY
				+ " NOT NULL", null, null);
		if (c == null)
			return;
		if (!c.moveToFirst()) {
			c.close();
			return;
		}
		mList = new ArrayList<ContentProviderOperation>(c.getCount());

		mHC = new DefaultHttpClient();
		for (int i = 0; i < c.getCount(); ++i) {
			try {
				this.fetch(String.valueOf(c.getLong(0)), c.getString(1));
			} catch (Exception e) {
				Log.d("gc", "", e);
			}
			c.moveToNext();
		}
		mHC.getConnectionManager().shutdown();
		try {
			if (!mList.isEmpty())
				cr.applyBatch(SquidProvider.AUTHORITY, mList);
		} catch (Exception e) {
		}
	}

	private void fetch(String id, String gaeKey) throws ParseException,
			ClientProtocolException, IOException, JSONException {
		HttpGet get = new HttpGet(SmogMobileAPI.CASE + "?"
				+ SmogMobileAPI.GAE_KEY + "=" + gaeKey);
		JSONObject jo = new JSONObject(EntityUtils.toString(this.mHC.execute(
				get).getEntity()));
		if (!jo.has(SmogMobileAPI.ERROR_KEY)) {
			ContentProviderOperation.Builder bdr = ContentProviderOperation
					.newUpdate(Squid.CONTENT_URI)
					.withSelection(Squid._ID + "=?", new String[] { id })
					.withValue(Squid.STATUS,
							jo.getString(SmogMobileAPI.Response.STATUS));
			String govId = jo.getString(SmogMobileAPI.Response.CASE_NUMBER);
			if (govId != null)
				bdr.withValue(Squid.GOV_ID, govId);
			mList.add(bdr.build());
		}
	}
}
