package lab.squidterminator.activity;

import lab.squidterminator.R;
import lab.squidterminator.fragment.PersonalInfoFragment;
import lab.squidterminator.fragment.ReportListFragment;
import lab.squidterminator.svcpvd.GovIdFetchService;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends SquidActivity implements Runnable {
	private static final String EXIST_KEY = "exist_key";

	private View mRoot;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mRoot = this.getLayoutInflater().inflate(R.layout.activity_main, null);
		this.setContentView(mRoot);
		if (savedInstanceState == null) {
			this.getSupportFragmentManager().beginTransaction()
					.replace(R.id.simple_fragment, new SplashScreenFragment())
					.commit();
			mRoot.postDelayed(this, 500);
		}
		this.startService(new Intent(this, GovIdFetchService.class));
	}

	@Override
	protected void onDestroy() {
		mRoot.removeCallbacks(this);
		super.onDestroy();
	}

	public static final class SplashScreenFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = new View(this.getActivity());
			view.setBackgroundResource(R.drawable.splashscreen);
			return view;
		}
	}

	public void run() {
		this.getSupportFragmentManager().beginTransaction()
				.replace(R.id.simple_fragment, new MainFragment())
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
				.commit();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(EXIST_KEY, true);
	}

	public static final class MainFragment extends Fragment implements
			OnClickListener {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_main, container, false);
		}

		private View mCamera, mCamcorder, mReport;
		private View mAllCases, mNotSended;

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			View view = this.getView();
			mCamera = view.findViewById(R.id.main_camera);
			mCamcorder = view.findViewById(R.id.main_camcorder);
			mReport = view.findViewById(R.id.main_report);
			mAllCases = view.findViewById(R.id.main_all_cases);
			mNotSended = view.findViewById(R.id.main_not_sended);
			//
			mCamera.setOnClickListener(this);
			mCamcorder.setOnClickListener(this);
			mReport.setOnClickListener(this);
			mAllCases.setOnClickListener(this);
			mNotSended.setOnClickListener(this);
			this.setHasOptionsMenu(true);
		}

		@Override
		public void onDestroyView() {
			mCamera.setOnClickListener(null);
			mCamcorder.setOnClickListener(null);
			mReport.setOnClickListener(null);
			mAllCases.setOnClickListener(null);
			mNotSended.setOnClickListener(null);
			//
			mCamera = null;
			mCamcorder = null;
			mReport = null;
			mAllCases = null;
			mNotSended = null;
			super.onDestroyView();
		}

		private static final int sMenuAbout = 500, sMenuPersonal = 501;

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			super.onCreateOptionsMenu(menu, inflater);
			menu.add(0, sMenuAbout, 0, R.string.menu_about).setIcon(
					android.R.drawable.ic_menu_info_details);
			menu.add(0, sMenuPersonal, 1, R.string.menu_prefs).setIcon(
					android.R.drawable.ic_menu_preferences);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			FragmentManager manager = this.getFragmentManager();
			FragmentTransaction ft = manager.beginTransaction();
			switch (item.getItemId()) {
			case sMenuAbout:
				ft.replace(R.id.simple_fragment, new SplashScreenFragment());
				break;
			case sMenuPersonal:
				Toast.makeText(this.getActivity(), R.string.toast_edit_pi_tips,
						Toast.LENGTH_SHORT).show();
				ft.replace(R.id.simple_fragment,
						PersonalInfoFragment.newInstance());
				break;
			default:
				throw new IllegalStateException("unknown item : " + item);
			}
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			ft.addToBackStack(null);
			ft.commit();
			return true;
		}

		private static final int REQUEST_IMAGE = 1000;
		private static final int REQUEST_VIDEO = 1001;
		private static final int REQUEST_CHOOSE = 1002;

		public void onClick(View v) {
			final int id = v.getId();
			Intent intent = new Intent();
			int request = 0;
			boolean getNotSended = false;
			switch (id) {
			case R.id.main_camera:
				intent.setAction(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
				request = REQUEST_IMAGE;
				break;
			case R.id.main_camcorder:
				intent.setAction(MediaStore.ACTION_VIDEO_CAPTURE);
				request = REQUEST_VIDEO;
				break;
			case R.id.main_report:
				intent.setAction(Intent.ACTION_GET_CONTENT);
				intent.setType("*/*");
				request = REQUEST_CHOOSE;
				break;
			case R.id.main_all_cases:
				getNotSended = false;
				break;
			case R.id.main_not_sended:
				getNotSended = true;
				break;
			default:
				throw new IllegalStateException("unknown id : " + id);
			}
			if (request != 0) {
				this.startActivityForResult(intent, request);
				return;
			}
			FragmentTransaction ft = this.getFragmentManager()
					.beginTransaction();
			Fragment fragment = ReportListFragment.newInstance(getNotSended);
			ft.replace(R.id.simple_fragment, fragment);
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			ft.addToBackStack(null);
			ft.commit();
			this.getActivity().startService(
					new Intent(this.getActivity(), GovIdFetchService.class));
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			Intent intent = new Intent();
			switch (requestCode) {
			case REQUEST_IMAGE:
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, REQUEST_CHOOSE);
				break;
			case REQUEST_VIDEO:
				intent.setType("video/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, REQUEST_CHOOSE);
				break;
			case REQUEST_CHOOSE:
				if (data != null && data.getData() != null) {
					intent.setClass(this.getActivity(),
							NavigationBarActivity.class);
					intent.putExtra(NavigationBarActivity.EXTRA_MEDIA_URI,
							data.getData());
					this.startActivity(intent);
				}
				break;
			default:
				throw new IllegalArgumentException("unknown requestCode : "
						+ requestCode);
			}
		}
	}
}