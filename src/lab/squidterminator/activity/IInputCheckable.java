package lab.squidterminator.activity;

public interface IInputCheckable {
	
	public boolean checkInputFields();
	
}
