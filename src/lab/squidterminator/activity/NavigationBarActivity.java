package lab.squidterminator.activity;

import lab.squidterminator.R;
import lab.squidterminator.SquidModel;
import lab.squidterminator.fragment.CarFindFragment;
import lab.squidterminator.fragment.MediaSelectFragment;
import lab.squidterminator.fragment.OverviewFragment;
import lab.squidterminator.fragment.PersonalInfoFragment;
import lab.squidterminator.svcpvd.UploadService;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class NavigationBarActivity extends SquidActivity implements
		OnClickListener, android.content.DialogInterface.OnClickListener {

	public static final String EXTRA_MEDIA_URI = "extra_media_uri";
	public static final String EXTRA_ID = "extra_record_id";

	public static boolean setTitle(Fragment frag, int res, int drawRes) {
		TextView tv = (TextView) frag.getActivity()
				.findViewById(R.id.nav_title);
		if (tv == null)
			return false;
		tv.setText(res);
		tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, drawRes);
		return true;
	}

	private static final int DIALOG_SAVE_CUURENT_CASE = 56414658;
	private static final String SAVED_COUNT = "saved_count_stack";

	private View mHome, mNext; //Navigation Bar

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		this.setContentView(R.layout.activity_navigationbar);
		mHome = findViewById(R.id.nav_home);
		mNext = findViewById(R.id.nav_next);
		mHome.setOnClickListener(this);
		mNext.setOnClickListener(this);
		//
		Fragment newFragment = null;
		Intent intent = this.getIntent();
		Uri uri = intent.getParcelableExtra(EXTRA_MEDIA_URI);
		long recordId = intent.getLongExtra(EXTRA_ID, -1);
		//
		SquidModel.newSinglecton();
		if (uri != null)
			newFragment = MediaSelectFragment.newInstance(uri);
		else if (recordId != -1)
			newFragment = MediaSelectFragment.newInstance(recordId);
		else
			throw new IllegalArgumentException("No uri and no id");
		if (arg0 == null) {
			FragmentTransaction ft = this.getSupportFragmentManager()
					.beginTransaction();
			ft.replace(R.id.simple_fragment, newFragment);
			ft.commit();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SAVED_COUNT, this.getSupportFragmentManager()
				.getBackStackEntryCount());
	}

	private static final String[] sFNames = new String[] {
			CarFindFragment.class.getName(),
			PersonalInfoFragment.class.getName(),
			OverviewFragment.class.getName(), };
	private static final int[] sErrMsgs = new int[] {
			R.string.toast_ms_basic_info,
			R.string.toast_incident_info_basic_info,
			R.string.toast_pi_basic_info, };

	public void onClick(View view) {
		FragmentManager manager = getSupportFragmentManager();
		int count = manager.getBackStackEntryCount();
		switch (view.getId()) {
		case R.id.nav_home:
			this.showDialog(DIALOG_SAVE_CUURENT_CASE);
			break;
		case R.id.nav_next:
			if (count == sFNames.length) {
				SquidModel model = SquidModel.getSinglecton();
				model.save(getContentResolver());
				//
				Intent service = new Intent(this, UploadService.class);
				service.putExtra(EXTRA_ID, model.getId());
				this.startService(service);
				this.finish();
			} else if (((IInputCheckable) manager
					.findFragmentById(R.id.simple_fragment)).checkInputFields()) {
				// Add the fragment to the activity, pushing this transaction
				// on to the back stack.
				FragmentTransaction ft = manager.beginTransaction();
				ft.replace(R.id.simple_fragment,
						Fragment.instantiate(this, sFNames[count]));
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				ft.addToBackStack(null).commit();
			} else
				Toast.makeText(this, sErrMsgs[count], Toast.LENGTH_SHORT)
						.show();
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == DIALOG_SAVE_CUURENT_CASE) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.dialog_save_case);
			builder.setPositiveButton(R.string.dialog_positive, this);
			builder.setNeutralButton(R.string.dialog_negative, this);
			builder.setNegativeButton(android.R.string.cancel, null);
			return builder.create();
		} else
			return this.onCreateDialog(id);
	}

	private boolean mIsSave;

	public void onClick(DialogInterface dialog, int which) {
		this.mIsSave = (which == DialogInterface.BUTTON_POSITIVE);
		this.finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.mHome.setOnClickListener(null);
		this.mNext.setOnClickListener(null);
		if (this.mIsSave) {
			Toast.makeText(this, R.string.toast_file_save, Toast.LENGTH_SHORT)
					.show();
			SquidModel.getSinglecton().save(getContentResolver());
		}
	}
}
