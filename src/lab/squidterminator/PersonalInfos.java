package lab.squidterminator;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * The class that use PreferenceManager.getDefaultSharedPreferences(context);
 * to storage the user preferences. It's useful when you combine this with PreferenceActivity.
 * 
 * Provide method for caller to access data, keep the keys in private, so we can move it to string.xml someday.
 * @author TomChen
 *
 */
public class PersonalInfos {

	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String MAIN_PHONE = "main_phone";
	private static final String OTHER_PHONE = "other_phone";
	private static final String ADDR_STATE_INDEX = "addr_state_index";
	private static final String ADDR_DISTRICT_INDEX = "addr_district_index";
	private static final String ADDRESS = "address";
	private static final String REPLY = "reply";
	private static final String REWARD = "reward";
	//Optional : Start with _
	private static final String _FAX_PHONE = "fax_phone";
	private static final String _EMAIL = "_email";
	private static final String _BANK_NAME = "_bank_name";
	private static final String _BANK_ACCOUNT = "_bank_account";

	private SharedPreferences mPref;
	private Editor mEditor;

	public PersonalInfos(Context context) {
		super();
		mPref = PreferenceManager.getDefaultSharedPreferences(context);
		mEditor = mPref.edit();
	}

	public boolean close() {
		try {
			return mEditor.commit();
		} finally {
			mPref = null;
			mEditor = null;
		}
	}

	public String getName() {
		return mPref.getString(NAME, null);
	}

	public String getID() {
		return mPref.getString(ID, null);
	}

	public String getMainPhone() {
		return mPref.getString(MAIN_PHONE, null);
	}

	public String getOtherPhone() {
		return mPref.getString(OTHER_PHONE, null);
	}

	public String getFaxPhone() {
		return mPref.getString(_FAX_PHONE, null);
	}

	public String getEmail() {
		return mPref.getString(_EMAIL, null);
	}

	public String getAddress() {
		return mPref.getString(ADDRESS, null);
	}

	public int getAddrStateIndex() {
		return mPref.getInt(ADDR_STATE_INDEX, 0);
	}

	public int getAddrDistrictIndex() {
		return mPref.getInt(ADDR_DISTRICT_INDEX, 0);
	}

	public int getRewardIndex() {
		return mPref.getInt(REWARD, 0);
	}

	public int getReplyIndex() {
		return mPref.getInt(REPLY, 0);
	}

	public int getPayIndex() {
		return 2;
	}

	public String getBankName() {
		return mPref.getString(_BANK_NAME, null);
	}

	public String getBankAccount() {
		return mPref.getString(_BANK_ACCOUNT, null);
	}

	public void setName(CharSequence name) {
		mEditor.putString(NAME,
				TextUtils.isEmpty(name) ? null : name.toString());
	}

	public void setID(CharSequence id) {
		mEditor.putString(ID, TextUtils.isEmpty(id) ? null : id.toString());
	}

	public void setMainPhone(CharSequence phone) {
		mEditor.putString(MAIN_PHONE,
				TextUtils.isEmpty(phone) ? null : phone.toString());
	}

	public void setOtherPhone(CharSequence night) {
		mEditor.putString(OTHER_PHONE,
				TextUtils.isEmpty(night) ? null : night.toString());
	}

	public void setFaxPhone(CharSequence fax) {
		mEditor.putString(_FAX_PHONE,
				TextUtils.isEmpty(fax) ? null : fax.toString());
	}

	public void setEmail(CharSequence email) {
		mEditor.putString(_EMAIL,
				TextUtils.isEmpty(email) ? null : email.toString());
	}

	public void setAddress(CharSequence addr) {
		mEditor.putString(ADDRESS,
				TextUtils.isEmpty(addr) ? null : addr.toString());
	}

	public void setAddrStateIndex(int index) {
		mEditor.putInt(ADDR_STATE_INDEX, index);
	}

	public void setAddrDistrictIndex(int index) {
		mEditor.putInt(ADDR_DISTRICT_INDEX, index);
	}

	public void setRewardIndex(int index) {
		mEditor.putInt(REWARD, index);
	}

	public void setReplyIndex(int index) {
		mEditor.putInt(REPLY, index);
	}

	public void setBankName(CharSequence bname) {
		mEditor.putString(_BANK_NAME,
				TextUtils.isEmpty(bname) ? null : bname.toString());
	}

	public void setBankAccount(CharSequence account) {
		mEditor.putString(_BANK_ACCOUNT, TextUtils.isEmpty(account) ? null
				: account.toString());
	}

	public ArrayList<NameValuePair> fillForm(Resources res) {
		this.mList = new ArrayList<NameValuePair>();
		this.add(SmogMobileAPI.Personal.NAME, getName());
		this.add(SmogMobileAPI.Personal.ID, getID());
		this.add(SmogMobileAPI.Personal.MAINTEL, getMainPhone());
		this.add(SmogMobileAPI.Personal.OPT_CELLPHONE, getMainPhone());
		this.add(SmogMobileAPI.Personal.ADDR_STATE,
				FromState.toStateCode(res, this.getAddrStateIndex()));
		this.add(SmogMobileAPI.Personal.ADDR_DISTRICT, FromState
				.toDistrictCode(getAddrStateIndex(), getAddrDistrictIndex()));
		this.add(SmogMobileAPI.Personal.ADDR, getAddress());
		//
		this.add(SmogMobileAPI.Personal.REPLY,
				"ABCDE".toCharArray()[getReplyIndex()]);
		this.add(SmogMobileAPI.Personal.REWARD, getRewardIndex());
		this.add(SmogMobileAPI.Personal.OPT_PAY, getPayIndex());
		this.add(SmogMobileAPI.Personal.OPT_EMAIL, getEmail());
		this.add(SmogMobileAPI.Personal.OPT_FAX, getFaxPhone());
		this.add(SmogMobileAPI.Personal.OPT_BANKNAME, getBankName());
		this.add(SmogMobileAPI.Personal.OPT_BANKACCOUNT, getBankAccount());
		return this.mList;
	}

	private void add(String key, Object value) {
		if (value != null)
			this.mList.add(new BasicNameValuePair(key, value.toString()));
	}

	private ArrayList<NameValuePair> mList;
}
