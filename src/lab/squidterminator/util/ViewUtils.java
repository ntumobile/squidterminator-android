package lab.squidterminator.util;

import android.view.View;


public class ViewUtils {
	@SuppressWarnings("unchecked")
	public static <T extends View> T find(View view, int id) {
		return (T) view.findViewById(id);
	}
}