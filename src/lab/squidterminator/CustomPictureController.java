package lab.squidterminator;

import lab.squidterminator.util.ViewUtils;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class CustomPictureController {

	private final RelativeLayout mRoot;
	private final ImageView mImage, mClose;
	private OnCloseClickListener mOnCloseClick;
	private OnImageClickListener mOnImageClick;
	private OnClick mOnClick = new OnClick();

	public CustomPictureController(RelativeLayout root) {
		super();
		this.mRoot = root;
		this.mImage = ViewUtils.find(root, R.id.customPictureImageView);
		this.mClose = ViewUtils.find(root, R.id.customPictureCloseImage);
		this.mImage.setOnClickListener(mOnClick);
		this.mClose.setOnClickListener(mOnClick);
	}

	public static interface OnCloseClickListener {

		public void onCloseClick(ImageView at, RelativeLayout root);
	}

	public void setOnCloseClickListener(OnCloseClickListener listener) {
		this.mOnCloseClick = listener;
	}

	public static interface OnImageClickListener {

		public void onImageClick(ImageView at, RelativeLayout root);
	}

	public void setOnImageClickListener(OnImageClickListener listener) {
		this.mOnImageClick = listener;
	}

	private class OnClick implements OnClickListener {
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.customPictureImageView:
				mOnImageClick.onImageClick(mImage, mRoot);
				break;
			case R.id.customPictureCloseImage:
				mRoot.setVisibility(View.INVISIBLE);
				mImage.setImageDrawable(null);
				mOnCloseClick.onCloseClick(mClose, mRoot);
				break;
			}
		}
	}

	public void setImageDrawable(Drawable d) {
		this.mRoot.setVisibility(View.VISIBLE);
		this.mImage.setImageDrawable(d);
	}

	public void setImageResource(int id) {
		this.mRoot.setVisibility(View.VISIBLE);
		this.mImage.setImageResource(id);
	}
	
	public boolean isShown(){
		return this.mRoot.getVisibility() == View.VISIBLE;
	}
}
