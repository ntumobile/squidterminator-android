package lab.squidterminator.fragment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import lab.squidterminator.CustomPictureController;
import lab.squidterminator.CustomPictureController.OnCloseClickListener;
import lab.squidterminator.CustomPictureController.OnImageClickListener;
import lab.squidterminator.R;
import lab.squidterminator.SquidModel;
import lab.squidterminator.activity.IInputCheckable;
import lab.squidterminator.activity.NavigationBarActivity;
import lab.squidterminator.util.ViewUtils;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

enum RTN_STATUS {
	OK, OK_ADDR_FOUND, FILE_TOO_LARGE, FAIL,
}

public class MediaSelectFragment extends Fragment implements OnClickListener,
		OnImageClickListener, OnCloseClickListener,
		LoaderCallbacks<RTN_STATUS>, IInputCheckable {
	public static Fragment newInstance(Uri mediaUri) {
		Fragment f = new MediaSelectFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelable(MEDIA_URI_KEY, mediaUri);
		f.setArguments(bundle);
		return f;
	}

	public static Fragment newInstance(long id) {
		Fragment f = new MediaSelectFragment();
		Bundle bundle = new Bundle();
		bundle.putLong(ID_KEY, id);
		f.setArguments(bundle);
		return f;
	}

	private static final String ID_KEY = "id_key";

	private static final int REQUEST_PICK_PHOTO = 4000;

	private static int[] sPicIds = new int[] { R.id.ms_image1, R.id.ms_image2,
			R.id.ms_image3, };

	private CustomPictureController[] mCustomPicture = new CustomPictureController[sPicIds.length];
	private Button mAdd;
	private EditText mPlate1, mPlate2;
	private Spinner mCarType;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.fragment_mediaselect, container, false);
	}

	private void findViews() {
		View view = this.getView();
		// image/text for image/video
		RelativeLayout root = null;
		CustomPictureController controller = null;
		for (int i = 0; i < mCustomPicture.length; ++i) {
			root = ViewUtils.find(view, sPicIds[i]);
			controller = new CustomPictureController(root);
			root.setTag(i);
			controller.setOnCloseClickListener(this);
			controller.setOnImageClickListener(this);
			this.mCustomPicture[i] = controller;
		}
		mAdd = ViewUtils.find(view, R.id.ms_add);
		mAdd.setOnClickListener(this);
		// text input
		mPlate1 = ViewUtils.find(view, R.id.ms_license_plate1);
		mPlate2 = ViewUtils.find(view, R.id.ms_license_plate2);
		mCarType = ViewUtils.find(view, R.id.ms_car_type);
	}

	private void populateFields() {
		SquidModel model = SquidModel.getSinglecton();
		//final String color = mCaseInfo.mCarColorMajor;
		//final boolean reward = mCaseInfo.mReward;
		int i = 0;
		if (model.getVideoUri() != null) {
			i = 1;
			this.mCustomPicture[0].setImageResource(R.drawable.video_thumbnail);
		}
		synchronized (model) {
			for (; i < mCustomPicture.length; ++i)
				this.setImageByMediaUri(i, model.getImageUri(i));
		}
		this.getIndex();//check
		mPlate1.setText(model.getLicensePlate1());
		mPlate2.setText(model.getLicensePlate2());
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.car_type_name, R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mCarType.setAdapter(adapter);
		mCarType.setSelection(model.getCarTypeSelection());
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Bundle args = this.getArguments();
		Uri mediaUri = args.getParcelable(MEDIA_URI_KEY);
		long id = args.getLong(ID_KEY, -1L);
		SquidModel model = SquidModel.getSinglecton();
		if (mediaUri != null) {
			if (mediaUri.getPathSegments().contains("video")) {
				model.setVideoUri(mediaUri);
			} else
				model.setImage(0, mediaUri);
		} else if (id != -1L) {
			model.getById(this.getActivity().getContentResolver(), id);
		}
		this.findViews();
		this.populateFields();
		NavigationBarActivity.setTitle(this, R.string.ms_title,
				R.drawable.steps_1);
	}

	public boolean checkInputFields() {
		return !TextUtils.isEmpty(this.mPlate1.getText())
				&& !TextUtils.isEmpty(this.mPlate2.getText());
	}

	@Override
	public void onStop() {
		super.onStop();
		SquidModel model = SquidModel.getSinglecton();
		model.setLicensePlate(mPlate1.getText(), mPlate2.getText());
		model.setCarTypeIdx(mCarType.getSelectedItemPosition());
	}

	public void onCloseClick(ImageView at, RelativeLayout root) {
		SquidModel model = SquidModel.getSinglecton();
		model.setImage((Integer) root.getTag(), null);
		this.getIndex();//check if we should show Add!!
	}

	public void onImageClick(ImageView at, RelativeLayout root) {
		Intent viewAction = new Intent(Intent.ACTION_VIEW);
		int index = (Integer) root.getTag();
		SquidModel model = SquidModel.getSinglecton();
		Uri uri = model.getImageUri(index);
		if (uri == null)
			uri = model.getVideoUri();
		viewAction.setData(uri);
		this.startActivity(viewAction);
	}

	public void onClick(View arg0) {
		Intent select = new Intent(Intent.ACTION_GET_CONTENT);
		select.setType("image/*");
		this.startActivityForResult(select, REQUEST_PICK_PHOTO);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_PICK_PHOTO:
			if (data != null && data.getData() != null) {
				Uri uri = data.getData();
				int index = this.getIndex();
				SquidModel.getSinglecton().setImage(index, uri);
				this.setImageByMediaUri(index, uri);
				this.getIndex();//check
			}
			break;
		}
	}

	private int getIndex() {
		int index = -1;
		for (int i = this.mCustomPicture.length - 1; i >= 0; --i)
			if (!this.mCustomPicture[i].isShown())
				index = i;
		final int newVis = index != -1 ? View.VISIBLE : View.INVISIBLE;
		this.mAdd.setVisibility(newVis);
		return index;
	}

	private void setImageByMediaUri(int index, Uri uri) {
		if (uri == null)
			return;
		Bundle args = new Bundle();
		args.putParcelable(MEDIA_URI_KEY, uri);
		this.getLoaderManager().restartLoader(REQUEST_PICK_PHOTO, args, this);
		try {
			InputStream is = this.getActivity().getContentResolver()
					.openInputStream(uri);
			Options opts = new Options();
			opts.inSampleSize = 8;
			this.mCustomPicture[index]
					.setImageDrawable(new BitmapDrawable(getResources(),
							BitmapFactory.decodeStream(is, null, opts)));
		} catch (FileNotFoundException e) {
		}
	}

	public void onLoaderReset(Loader<RTN_STATUS> arg0) {
	}

	public void onLoadFinished(Loader<RTN_STATUS> arg0, RTN_STATUS arg1) {
		switch (arg1) {
		case OK:
			break;
		case OK_ADDR_FOUND:
			Toast.makeText(getActivity(), R.string.ms_address_found,
					Toast.LENGTH_SHORT).show();
			break;
		case FILE_TOO_LARGE:
			Toast.makeText(this.getActivity(),
					this.getString(R.string.ms_file_too_large, sFileMaxSizeMB),
					Toast.LENGTH_SHORT).show();
			this.getActivity().finish();
			break;
		case FAIL:
			Toast.makeText(this.getActivity(), R.string.ms_uri_fail,
					Toast.LENGTH_SHORT).show();
			this.getActivity().finish();
		}
	}

	public Loader<RTN_STATUS> onCreateLoader(int arg0, Bundle arg1) {
		return new MediaLoader(this.getActivity(),
				(Uri) arg1.getParcelable(MEDIA_URI_KEY));
	}

	private static final String MEDIA_URI_KEY = "media_uri_key";
	private static final String[] MEDIA_COLUMNS = new String[] { MediaStore.MediaColumns.SIZE, };
	private static final String[] IMAGE_COLUMNS = new String[] {
			MediaStore.Images.Media.LATITUDE,
			MediaStore.Images.Media.LONGITUDE,
			MediaStore.Images.Media.DATE_TAKEN, };

	private static final int COLUMN_LAT = 0, COLUMN_LNG = 1, COLUMN_DATE = 2;

	private static final int sFileMaxSizeMB = 1;
	private static final long sFileMaxSize = 1024 * 1024 * sFileMaxSizeMB;

	private static class MediaLoader extends AsyncTaskLoader<RTN_STATUS> {
		private final Uri mMediaUri;

		public MediaLoader(Context context, Uri mediaUri) {
			super(context);
			this.mMediaUri = mediaUri;
		}

		@Override
		protected void onStartLoading() {
			this.forceLoad();
		}

		@Override
		public RTN_STATUS loadInBackground() {
			Context context = this.getContext();
			ContentResolver cr = context.getContentResolver();
			Cursor media = cr.query(mMediaUri, MEDIA_COLUMNS, null, null, null);
			if (media == null)
				return RTN_STATUS.FAIL;
			try {
				if (media.moveToFirst()) {
					long size = media.getLong(0);
					if (size >= sFileMaxSize)
						return RTN_STATUS.FILE_TOO_LARGE;
				}
			} finally {
				media.close();
			}
			Cursor image = cr.query(mMediaUri, IMAGE_COLUMNS, null, null, null);
			if (image == null)
				return RTN_STATUS.FAIL;
			try {
				if (image.moveToFirst()) {
					SquidModel model = SquidModel.getSinglecton();
					if (model.addressExists())
						return RTN_STATUS.OK;
					model.setGEO(image.getDouble(COLUMN_LAT),
							image.getDouble(COLUMN_LNG));
					model.setDate(image.getLong(COLUMN_DATE));
					boolean exceptionRaised = true;
					while (exceptionRaised)
						try {
							model.findAddress(context);
							exceptionRaised = false;
						} catch (IOException e) {
						}
					return RTN_STATUS.OK_ADDR_FOUND;
				}
			} finally {
				image.close();
			}
			return RTN_STATUS.FAIL;
		}
	}
}
