package lab.squidterminator.fragment;

import java.util.Date;

import lab.squidterminator.R;
import lab.squidterminator.SquidModel;
import lab.squidterminator.activity.NavigationBarActivity;
import lab.squidterminator.svcpvd.GovIdFetchService;
import lab.squidterminator.svcpvd.SquidProvider.Squid;
import lab.squidterminator.util.ViewUtils;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.ResourceCursorAdapter;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ReportListFragment extends ListFragment implements
		LoaderCallbacks<Cursor>, OnClickListener {

	public static Fragment newInstance(boolean getNotSended) {
		Fragment f = new ReportListFragment();
		Bundle b = new Bundle();
		b.putBoolean(NOT_SENDED_KEY, getNotSended);
		f.setArguments(b);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_reportlist, container, false);
	}

	private static final String NOT_SENDED_KEY = "not_sended_key";
	private static final int sCursorLoader = 700;
	private static final int sRefreshId = 701;

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Boolean notSent = (Boolean) v.getTag();
		if (notSent) {
			Intent intent = new Intent(this.getActivity(),
					NavigationBarActivity.class);
			intent.putExtra(NavigationBarActivity.EXTRA_ID, id);
			this.startActivity(intent);
		} else {
			FragmentManager manager = this.getFragmentManager();
			FragmentTransaction ft = manager.beginTransaction();
			ft.replace(R.id.simple_fragment, OverviewFragment.newInstance(id));
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			ft.addToBackStack(null);
			ft.commit();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		View view = this.getView();
		view.findViewById(R.id.nav_next).setEnabled(false);
		view.findViewById(R.id.nav_home).setOnClickListener(this);
		int resid = this.getArguments().getBoolean(NOT_SENDED_KEY) ? R.string.report_list_not_sended_title
				: R.string.report_list_all_case_title;
		((TextView) view.findViewById(R.id.nav_title)).setText(resid);
		this.setListAdapter(new RecordAdapter(this));
		this.getLoaderManager().initLoader(sCursorLoader, this.getArguments(),
				this);
		this.setHasOptionsMenu(true);
	}

	public void onClick(View v) {
		this.getFragmentManager().popBackStack();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.add(0, sRefreshId, 0, R.string.menu_refresh).setIcon(
				R.drawable.ic_menu_refresh);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case sRefreshId:
			this.getActivity().startService(
					new Intent(this.getActivity(), GovIdFetchService.class));
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	public RecordAdapter getListAdapter() {
		return (RecordAdapter) super.getListAdapter();
	}

	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		this.getListAdapter().swapCursor(arg1);
	}

	public void onLoaderReset(Loader<Cursor> arg0) {
		this.getListAdapter().swapCursor(null);
	}

	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		String selection = null;
		if (arg1.getBoolean(NOT_SENDED_KEY))
			selection = Squid.GAE_KEY + " IS NULL";
		return new CursorLoader(getActivity(), Squid.CONTENT_URI, sProjection,
				selection, null, null);
	}

	private static final String[] sProjection = new String[] { Squid._ID,
			Squid.VIDEO, Squid.THUMBNAIL1, Squid.THUMBNAIL2, Squid.THUMBNAIL3,
			Squid.STATE_INDEX, Squid.DISTRICT_INDEX, Squid.ADDRESS, Squid.DATE,
			Squid.STATUS, Squid.GAE_KEY, Squid.GOV_ID, };

	private static class RecordAdapter extends ResourceCursorAdapter {
		private static final int COLUMN_FIELD_VIDEO = 1;
		private static final int COLUMN_FIELD_THUMBNAIL1 = 2;
		private static final int COLUMN_FIELD_THUMBNAIL3 = 4;
		private static final int COLUMN_FIELD_STATE = 5;
		private static final int COLUMN_FIELD_DISTRICT = 6;
		private static final int COLUMN_FIELD_ADDRESS = 7;
		private static final int COLUMN_FIELD_DATE = 8;
		private static final int COLUMN_FIELD_STATUS = 9;
		private static final int COLUMN_FIELD_GAE_KEY = 10;
		private static final int COLUMN_FIELD_GOV_ID = 11;

		public RecordAdapter(ReportListFragment f) {
			super(f.getActivity(), R.layout.report_list_item, null, false);
		}

		/**
		 * Hide the view showing detail in this adapter, so the caller only need to setAdapter().
		 */
		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			final Date d = new Date(cursor.getLong(COLUMN_FIELD_DATE));
			getDate(view).setText(DateFormat.getDateFormat(context).format(d));
			//
			final SquidModel model = SquidModel.createInstance();
			final String videoUri = cursor.getString(COLUMN_FIELD_VIDEO);
			boolean setDefault = (videoUri != null);
			if (!setDefault) {
				String uriStr = null;
				for (int i = 0, keyIdx = COLUMN_FIELD_THUMBNAIL1; keyIdx <= COLUMN_FIELD_THUMBNAIL3; ++i, ++keyIdx) {
					uriStr = cursor.getString(keyIdx);
					if (uriStr != null)
						model.setImage(i, Uri.parse(uriStr));
				}
				if (!model.setImageDrawable(getPhoto1(view), context))
					setDefault = true;
			}
			if (setDefault)
				getPhoto1(view).setImageResource(R.drawable.video_thumbnail);
			model.setStateIdx(cursor.getInt(COLUMN_FIELD_STATE));
			model.setDistrictIdx(cursor.getInt(COLUMN_FIELD_DISTRICT));
			model.setShortAddress(cursor.getString(COLUMN_FIELD_ADDRESS));
			getAddr(view).setText(model.getFullAddress(context.getResources()));
			//
			final TextView statusTV = getStatus(view);
			final String status = cursor.getString(COLUMN_FIELD_STATUS);
			if (TextUtils.isEmpty(status))
				statusTV.setText(R.string.report_list_not_sent);
			else
				statusTV.setText(status);
			//
			view.setTag(TextUtils.isEmpty(cursor
					.getString(COLUMN_FIELD_GAE_KEY)));
			//
			final TextView govTV = getGovId(view);
			final String govId = cursor.getString(COLUMN_FIELD_GOV_ID);
			if (TextUtils.isEmpty(govId))
				govTV.setVisibility(View.GONE);
			else {
				govTV.setVisibility(View.VISIBLE);
				govTV.setText(context.getString(R.string.report_list_id_name,
						govId));
			}
		}

		private static ImageView getPhoto1(View view) {
			return ViewUtils.find(view, R.id.report_list_item_photo1);
		}

		private static TextView getDate(View view) {
			return ViewUtils.find(view, R.id.report_list_item_date);
		}

		private static TextView getAddr(View view) {
			return ViewUtils.find(view, R.id.report_list_item_addr);
		}

		private static TextView getStatus(View view) {
			return ViewUtils.find(view, R.id.report_list_item_status);
		}

		private static TextView getGovId(View view) {
			return ViewUtils.find(view, R.id.report_list_item_gov_id);
		}
	}
}
