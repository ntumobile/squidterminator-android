package lab.squidterminator.fragment;

import lab.squidterminator.FromState;
import lab.squidterminator.PersonalInfos;
import lab.squidterminator.R;
import lab.squidterminator.activity.IInputCheckable;
import lab.squidterminator.activity.NavigationBarActivity;
import lab.squidterminator.util.ViewUtils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class PersonalInfoFragment extends Fragment implements
		OnItemSelectedListener, IInputCheckable, Runnable {
	public static Fragment newInstance() {
		return new PersonalInfoFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_personal, container, false);
	}

	//Personal Info View
	private EditText mName, mId, mMainNumber, mAddress;
	private Spinner mAddressState, mAddressDistrict;
	private Spinner mReplyMethod, mRewardMethod;
	//above are necessary
	private EditText mBankName, mBankAccount;
	private EditText mOtherNumber, mFax;
	private EditText mEmail;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.preparePersonalInfoView();
		NavigationBarActivity.setTitle(this, R.string.pi_title,
				R.drawable.steps_3);
	}

	//private EditText mPMobileNumber;
	private void preparePersonalInfoView() {
		View view = this.getView();
		PersonalInfos info = new PersonalInfos(this.getActivity());
		mName = ViewUtils.find(view, R.id.pi_name);
		mName.setText(info.getName());
		//
		mId = ViewUtils.find(view, R.id.pi_id);
		mId.setText(info.getID());
		//
		mMainNumber = ViewUtils.find(view, R.id.pi_day_number);
		mMainNumber.setText(info.getMainPhone());
		//
		mOtherNumber = ViewUtils.find(view, R.id.pi_night_number);
		mOtherNumber.setText(info.getOtherPhone());
		//
		//mPMobileNumber = (EditText) findViewById(R.id.pi_mobile_number);
		//mPMobileNumber.setText(info.getPhoneMobile());
		//
		mFax = ViewUtils.find(view, R.id.pi_fax);
		mFax.setText(info.getFaxPhone());
		//
		mEmail = ViewUtils.find(view, R.id.pi_email);
		mEmail.setText(info.getEmail());
		//
		mAddress = ViewUtils.find(view, R.id.pi_address);
		mAddress.setText(info.getAddress());
		//
		mAddressState = ViewUtils.find(view, R.id.pi_address_state);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.state_names, R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mAddressState.setAdapter(adapter);
		mAddressDistrict = ViewUtils.find(view, R.id.pi_address_distric);
		FromState.initSelection(mAddressState, info.getAddrStateIndex(),
				mAddressDistrict, info.getAddrDistrictIndex());
		//		
		mReplyMethod = ViewUtils.find(view, R.id.pi_reply_method);
		adapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.IsReply_name, R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mReplyMethod.setAdapter(adapter);
		mReplyMethod.setOnItemSelectedListener(this);
		mReplyMethod.setSelection(info.getReplyIndex());
		//
		mRewardMethod = ViewUtils.find(view, R.id.pi_reward_method);
		adapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.IsGetMoney_name, R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mRewardMethod.setAdapter(adapter);
		mRewardMethod.setOnItemSelectedListener(this);
		mRewardMethod.setSelection(info.getRewardIndex());
		//
		mBankName = ViewUtils.find(view, R.id.pi_bank_name);
		mBankName.setText(info.getBankName());
		//
		mBankAccount = ViewUtils.find(view, R.id.pi_bank_account);
		mBankAccount.setText(info.getBankAccount());
		info.close();
	}

	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		View view = this.getView();
		switch (arg0.getId()) {
		case R.id.pi_reply_method:
			final int vis = (arg2 > 2 ? View.VISIBLE : View.GONE);
			view.findViewById(R.id.pi_email_row).setVisibility(vis);
			break;
		case R.id.pi_reward_method:
			final int vis1 = (arg2 > 1 ? View.VISIBLE : View.GONE);
			view.findViewById(R.id.pi_bank_name_row).setVisibility(vis1);
			view.findViewById(R.id.pi_bank_account_row).setVisibility(vis1);
			break;
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public boolean checkInputFields() {
		return !TextUtils.isEmpty(mName.getText())
				&& !TextUtils.isEmpty(mId.getText())
				&& !TextUtils.isEmpty(mMainNumber.getText())
				&& !TextUtils.isEmpty(mAddress.getText());
	}

	@Override
	public void onStop() {
		super.onStop();
		new Thread(this).start();
		Toast.makeText(this.getActivity(), R.string.toast_pi_save,
				Toast.LENGTH_SHORT).show();

	}

	public void run() {
		PersonalInfos info = new PersonalInfos(this.getActivity());
		info.setName(mName.getText());
		info.setID(mId.getText());
		info.setMainPhone(mMainNumber.getText());
		info.setOtherPhone(mOtherNumber.getText());
		//info.setPhoneMobile(mPMobileNumber.getText());
		info.setFaxPhone(mFax.getText());
		info.setEmail(mEmail.getText());
		info.setAddress(mAddress.getText());
		//
		info.setAddrStateIndex(mAddressState.getSelectedItemPosition());
		info.setAddrDistrictIndex(mAddressDistrict.getSelectedItemPosition());
		//
		info.setReplyIndex(mReplyMethod.getSelectedItemPosition());
		info.setRewardIndex(mRewardMethod.getSelectedItemPosition());
		//
		info.setBankName(mBankName.getText());
		info.setBankAccount(mBankAccount.getText());
		info.close();
	}
}
