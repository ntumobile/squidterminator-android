package lab.squidterminator.fragment;

import java.util.Calendar;

import lab.squidterminator.FromState;
import lab.squidterminator.R;
import lab.squidterminator.SquidModel;
import lab.squidterminator.activity.IInputCheckable;
import lab.squidterminator.activity.NavigationBarActivity;
import lab.squidterminator.util.ViewUtils;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

public class CarFindFragment extends Fragment implements IInputCheckable,
		OnClickListener, OnDateSetListener, OnTimeSetListener {

	public static Fragment newInstance() {
		return new CarFindFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_carfind, container, false);
	}

	// Incident Info View
	private Spinner mState, mDistrict;
	private EditText mAddress;
	private TextView mTextView;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		NavigationBarActivity.setTitle(this, R.string.cf_title,
				R.drawable.steps_2);
		/*
		 * Init views
		 */
		View view = this.getView();
		this.mState = ViewUtils.find(view, R.id.cf_addr_state);
		this.mDistrict = ViewUtils.find(view, R.id.cf_addr_district);
		this.mAddress = ViewUtils.find(view, R.id.cf_addr);
		this.mTextView = ViewUtils.find(view, R.id.cf_date);
		ViewUtils.find(view, R.id.cf_changedate).setOnClickListener(this);
		//
		SquidModel model = SquidModel.getSinglecton();
		FromState.initSelection(mState, model.getStateIdx(), mDistrict,
				model.getDistrictIdx());
		this.mAddress.setText(model.getShortAddress());
		this.setDate(model.getDate());
	}

	private void setDate(long time) {
		this.mTextView.setText(DateUtils.formatDateTime(getActivity(), time,
				DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));
	}

	public boolean checkInputFields() {
		return !TextUtils.isEmpty(this.mAddress.getText());
	}

	@Override
	public void onStop() {
		super.onStop();
		SquidModel model = SquidModel.getSinglecton();
		model.setStateIdx(mState.getSelectedItemPosition());
		model.setDistrictIdx(mDistrict.getSelectedItemPosition());
		model.setShortAddress(mAddress.getText());
	}

	@Override
	public void onClick(View view) {
		Calendar calendar = Calendar.getInstance();
		SquidModel model = SquidModel.getSinglecton();
		final long date = model.getDate();
		if (date > 0) {
			calendar.setTimeInMillis(date);
		}
		new DatePickerDialog(getActivity(), this, calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH)).show();

	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, view.getYear());
		calendar.set(Calendar.MONTH, view.getMonth());
		calendar.set(Calendar.DAY_OF_MONTH, view.getDayOfMonth());
		SquidModel model = SquidModel.getSinglecton();
		model.setDate(calendar.getTimeInMillis());
		new TimePickerDialog(getActivity(), this,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), false).show();
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		Calendar calendar = Calendar.getInstance();
		SquidModel model = SquidModel.getSinglecton();
		calendar.setTimeInMillis(model.getDate());
		calendar.set(Calendar.HOUR_OF_DAY, view.getCurrentHour());
		calendar.set(Calendar.MINUTE, view.getCurrentMinute());
		model.setDate(calendar.getTimeInMillis());
		this.setDate(model.getDate());
	}
}
