package lab.squidterminator.fragment;

import java.util.Calendar;
import java.util.Date;

import lab.squidterminator.R;
import lab.squidterminator.SquidModel;
import lab.squidterminator.activity.NavigationBarActivity;
import lab.squidterminator.util.ViewUtils;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class OverviewFragment extends Fragment implements
		DialogInterface.OnClickListener {

	public static Fragment newInstance(long recordId) {
		Fragment f = new OverviewFragment();
		Bundle args = new Bundle();
		args.putLong(FROM_RECORD, recordId);
		f.setArguments(args);
		return f;
	}

	private static final String FROM_RECORD = "from_record";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_overview, container, false);
	}

	private ImageView mThumbnail;
	private TextView mLicense, mCarType;
	private TextView mAddress, mDate;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Bundle args = this.getArguments();
		if (args != null) {
			long id = args.getLong(FROM_RECORD, -1L);
			SquidModel.newSinglecton().getById(
					this.getActivity().getContentResolver(), id);
			this.setHasOptionsMenu(true);
		} else
			NavigationBarActivity.setTitle(this, R.string.ov_title,
					R.drawable.steps_4);
		this.prepareov();
	}

	private void prepareov() {
		View view = this.getView();
		mThumbnail = ViewUtils.find(view, R.id.ov_thumbnail);
		mLicense = ViewUtils.find(view, R.id.ov_license);
		mCarType = ViewUtils.find(view, R.id.ov_car_type);
		mAddress = ViewUtils.find(view, R.id.ov_address);
		mDate = ViewUtils.find(view, R.id.ov_date);
		//
		SquidModel model = SquidModel.getSinglecton();
		Uri videoUri = model.getVideoUri();
		boolean setDefault = (videoUri != null);
		if (!setDefault) {
			if (!model.setImageDrawable(mThumbnail, getActivity()))
				setDefault = true;
		}
		if (setDefault)
			this.mThumbnail.setImageResource(R.drawable.video_thumbnail);
		mLicense.setText(model.getLicencePlateFull());
		Resources res = this.getResources();
		mCarType.setText(res.getStringArray(R.array.car_type_name)[model
				.getCarTypeSelection()]);
		mAddress.setText(model.getFullAddress(res));
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.setTimeInMillis(model.getDate());
		Date date = mCalendar.getTime();
		mDate.setText(date.toLocaleString());
	}

	private static final int sMenuDelete = 600;

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.add(0, sMenuDelete, 0, R.string.ov_delete).setIcon(
				android.R.drawable.ic_menu_delete);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case sMenuDelete:
			AlertDialog.Builder builder = new AlertDialog.Builder(
					this.getActivity());
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage(R.string.dialog_delete_case);
			builder.setPositiveButton(R.string.dialog_positive, this);
			builder.setNegativeButton(R.string.dialog_negative, null);
			builder.show();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	public void onClick(DialogInterface dialog, int which) {
		final int rows = SquidModel.getSinglecton().deleteThis(
				this.getActivity().getContentResolver());

		final int msg = (rows > 0 ? R.string.toast_case_delete_success
				: R.string.toast_case_delete_fail);
		Toast.makeText(this.getActivity(), msg, Toast.LENGTH_SHORT).show();
		this.getFragmentManager().popBackStack();
	}
}
