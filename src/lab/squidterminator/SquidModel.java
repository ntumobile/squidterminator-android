package lab.squidterminator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import lab.squidterminator.svcpvd.SquidProvider.Squid;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.widget.ImageView;

public class SquidModel {
	public static SquidModel getSinglecton() {
		return sModel;
	}

	public static SquidModel newSinglecton() {
		return sModel = new SquidModel();
	}

	public static SquidModel createInstance() {
		return new SquidModel();
	}

	private static SquidModel sModel;

	public SquidModel getById(ContentResolver cr, long id) {
		Cursor c = cr.query(
				ContentUris.withAppendedId(Squid.CONTENT_ITEM_URI, id), null,
				null, null, null);
		if (c != null) {
			try {
				if (c.moveToFirst())
					this.readFrom(c);
			} finally {
				c.close();
			}
		}
		return this;
	}

	public int deleteThis(ContentResolver cr) {
		return cr.delete(
				ContentUris.withAppendedId(Squid.CONTENT_ITEM_URI, this.mId),
				null, null);
	}

	private SquidModel readFrom(Cursor cursor) {
		mId = cursor.getLong(COLUMN_ID);
		if (cursor.getString(COLUMN_VIDEO) != null)
			this.mVideoUri = Uri.parse(cursor.getString(COLUMN_VIDEO));
		for (int i = 0, keyIdx = COLUMN_THUMBNAIL_1; keyIdx <= COLUMN_THUMBNAIL_3; ++i, ++keyIdx) {
			if (cursor.getString(keyIdx) != null)
				mImageUris[i] = Uri.parse(cursor.getString(keyIdx));
		}
		mLng = cursor.getFloat(COLUMN_LONGITUDE);
		mLat = cursor.getFloat(COLUMN_LATITUDE);
		mDate = cursor.getLong(COLUMN_DATE);
		mStateIndex = cursor.getInt(COLUMN_STATE_INDEX);
		mDistrictIndex = cursor.getInt(COLUMN_DISTRICT_INDEX);
		mAddress = cursor.getString(COLUMN_ADDRESS);
		//
		mLicensePlate1 = cursor.getString(COLUMN_CAR_LICENSE1);
		mLicensePlate2 = cursor.getString(COLUMN_CAR_LICENSE2);
		mCarTypeIndex = cursor.getInt(COLUMN_CAR_TYPE_INDEX);
		//
		mGaeKey = cursor.getString(COLUMN_GAE_KEY);
		mStatus = cursor.getString(COLUMN_STATUS);
		mGovId = cursor.getString(COLUMN_GOV_ID);
		return this;
	}

	//
	private static final int COLUMN_ID = 0;
	private static final int COLUMN_VIDEO = 1;
	private static final int COLUMN_THUMBNAIL_1 = 2;
	private static final int COLUMN_THUMBNAIL_3 = 4;
	//
	private static final int COLUMN_LONGITUDE = 5;
	private static final int COLUMN_LATITUDE = 6;
	private static final int COLUMN_DATE = 7;
	private static final int COLUMN_STATE_INDEX = 8;
	private static final int COLUMN_DISTRICT_INDEX = 9;
	private static final int COLUMN_ADDRESS = 10;
	//
	private static final int COLUMN_CAR_LICENSE1 = 11;
	private static final int COLUMN_CAR_LICENSE2 = 12;
	private static final int COLUMN_CAR_TYPE_INDEX = 13;
	//
	private static final int COLUMN_GAE_KEY = 14;
	private static final int COLUMN_STATUS = 15;
	private static final int COLUMN_GOV_ID = 16;
	//
	private static final String[] sThumbnailKeys = new String[] {
			Squid.THUMBNAIL1, Squid.THUMBNAIL2, Squid.THUMBNAIL3, };

	public void save(ContentResolver cr) {
		ContentValues cv = new ContentValues();
		//
		Uri uri = this.getVideoUri();
		if (uri != null) {
			cv.put(Squid.VIDEO, uri.toString());
		}
		for (int i = 0; i < MEDIA_COUNT; i++) {
			uri = this.getImageUri(i);
			if (uri != null)
				cv.put(sThumbnailKeys[i], uri.toString());
			else
				cv.putNull(sThumbnailKeys[i]);
		}
		cv.put(Squid.LONGITUDE, this.getLng());
		cv.put(Squid.LATITUDE, this.getLat());
		cv.put(Squid.DATE, this.getDate());
		cv.put(Squid.STATE_INDEX, this.getStateIdx());
		cv.put(Squid.DISTRICT_INDEX, this.getDistrictIdx());
		cv.put(Squid.ADDRESS, this.getShortAddress());
		//
		cv.put(Squid.CAR_LICENSE1, this.getLicensePlate1());
		cv.put(Squid.CAR_LICENSE2, this.getLicensePlate2());
		cv.put(Squid.CAR_TYPE_INDEX, this.getCarTypeSelection());
		//
		cv.put(Squid.STATUS, this.getStatus());
		cv.put(Squid.GAE_KEY, this.getGAEKey());
		cv.put(Squid.GOV_ID, this.getGovId());
		//
		if (this.mId != -1L) {
			cr.update(ContentUris.withAppendedId(Squid.CONTENT_ITEM_URI,
					this.mId), cv, null, null);
		} else {
			uri = cr.insert(Squid.CONTENT_URI, cv);
			this.mId = ContentUris.parseId(uri);
		}
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder(1024);
		b.append("mId: ").append(getId()).append('\n');
		//
		b.append("video : ").append(String.valueOf(mVideoUri)).append('\n');
		for (int i = 0; i < MEDIA_COUNT; ++i)
			b.append("image ").append(i).append(": ").append(mImageUris[i])
					.append('\n');
		//
		b.append("mLng: ").append(mLng).append('\n');
		b.append("mLat: ").append(mLat).append('\n');
		b.append("mDate: ").append(mDate).append('\n');
		b.append("mAddress: ").append(mAddress).append('\n');
		b.append("mStateIndex: ").append(mStateIndex).append('\n');
		b.append("mDistrictIndex: ").append(mDistrictIndex).append('\n');
		//
		b.append("mLicensePlate1: ").append(mLicensePlate1).append('\n');
		b.append("mLicensePlate2: ").append(mLicensePlate2).append('\n');
		b.append("mCarTypeIndex: ").append(mCarTypeIndex).append('\n');
		//
		b.append("mGaeKey: ").append(mGaeKey).append('\n');
		b.append("mStatus: ").append(mStatus).append('\n');
		b.append("mGovId: ").append(mGovId).append('\n');
		return b.toString();
	}

	private long mId = -1L;
	private Uri mVideoUri;
	private Uri[] mImageUris = new Uri[MEDIA_COUNT];
	//
	private double mLat, mLng;
	private long mDate;
	private int mStateIndex, mDistrictIndex;
	private String mAddress;
	//
	private String mLicensePlate1, mLicensePlate2;
	private int mCarTypeIndex;
	//
	private String mGaeKey, mStatus, mGovId;

	public static final int MEDIA_COUNT = 3;

	private SquidModel() {
		super();
	}

	public synchronized void setImage(int index, Uri imageUri) {
		this.mImageUris[index] = imageUri;
	}

	public void setVideoUri(Uri uri) {
		this.mVideoUri = uri;
	}

	public void setGEO(double lat, double lng) {
		if (lat != 0d && lng != 0d) {
			this.mLat = lat;
			this.mLng = lng;
		}
	}

	public boolean addressExists() {
		return !TextUtils.isEmpty(this.mAddress);
	}

	public void findAddress(Context context) throws IOException {
		Geocoder gc = new Geocoder(context, Locale.getDefault());
		List<Address> results = gc.getFromLocation(mLat, mLng, 1);
		if (results.isEmpty())
			throw new IOException("no result found");
		//
		Address address = results.get(0);
		String addr = address.getAddressLine(0);
		String dist = address.getLocality();
		int idxOfDist = addr.indexOf(dist);
		if (idxOfDist != -1)
			idxOfDist += dist.length();
		else
			idxOfDist = 0;
		//
		this.mAddress = addr.substring(idxOfDist);
		String stateStr = address.getAdminArea(), disStr = address
				.getLocality();
		Resources res = context.getResources();
		String[] stateNames = res.getStringArray(R.array.state_names);
		int stateIdx = 0;
		for (; stateIdx < stateNames.length; ++stateIdx)
			if (stateNames[stateIdx].equals(stateStr))
				break;
		this.mStateIndex = stateIdx;
		String[] disNames = res.getStringArray(FromState
				.toDistrictNames(stateIdx));
		for (int i = 0; i < disNames.length; ++i)
			if (disNames[i].equals(disStr))
				this.mDistrictIndex = i;
	}

	public void setDate(long date) {
		if (date != 0L)
			this.mDate = date;
	}

	public void setStateIdx(int idx) {
		this.mStateIndex = idx;
	}

	public void setDistrictIdx(int idx) {
		this.mDistrictIndex = idx;
	}

	public void setShortAddress(CharSequence address) {
		if (!TextUtils.isEmpty(address))
			this.mAddress = address.toString();
	}

	public void setLicensePlate(CharSequence prefix, CharSequence postfix) {
		if (TextUtils.isEmpty(prefix) || TextUtils.isEmpty(postfix))
			return;
		mLicensePlate1 = prefix.toString();
		mLicensePlate2 = postfix.toString();
	}

	public void setCarTypeIdx(int idx) {
		mCarTypeIndex = idx;
	}

	public void setStatus(String status) {
		mStatus = status;
	}

	public void setGAEKey(String gaekey) {
		mGaeKey = gaekey;
	}

	public long getId() {
		return this.mId;
	}

	public synchronized Uri getImageUri(int index) {
		return this.mImageUris[index];
	}

	public boolean setImageDrawable(ImageView iv, Context context) {
		Uri uri = null;
		for (int i = 0; i < SquidModel.MEDIA_COUNT; ++i) {
			uri = this.getImageUri(i);
			if (uri != null)
				break;
		}
		if (uri != null) {
			try {
				InputStream is = context.getContentResolver().openInputStream(
						uri);
				Options opts = new Options();
				opts.inSampleSize = 8;

				iv.setImageDrawable(new BitmapDrawable(context.getResources(),
						BitmapFactory.decodeStream(is, null, opts)));
				return true;
			} catch (FileNotFoundException e) {
			}
		}
		return false;
	}

	public Uri getVideoUri() {
		return this.mVideoUri;
	}

	public long getDate() {
		return this.mDate;
	}

	public double getLat() {
		return this.mLat;
	}

	public double getLng() {
		return this.mLng;
	}

	public int getStateIdx() {
		return this.mStateIndex;
	}

	public int getDistrictIdx() {
		return this.mDistrictIndex;
	}

	public String getShortAddress() {
		return this.mAddress;
	}

	public String getFullAddress(Resources res) {
		StringBuilder builder = new StringBuilder();
		builder.append(res.getStringArray(R.array.state_names)[this.mStateIndex]);
		final int districtRes = FromState.toDistrictNames(mStateIndex);
		builder.append(res.getStringArray(districtRes)[this.mDistrictIndex]);
		builder.append(this.mAddress);
		return builder.toString();
	}

	public String getLicensePlate1() {
		return this.mLicensePlate1;
	}

	public String getLicensePlate2() {
		return this.mLicensePlate2;
	}

	public String getLicencePlateFull() {
		return this.mLicensePlate1 + '-' + this.mLicensePlate2;
	}

	public int getCarTypeSelection() {
		return this.mCarTypeIndex;
	}

	public String getGAEKey() {
		return mGaeKey;
	}

	public String getStatus() {
		return this.mStatus;
	}

	public String getGovId() {
		return this.mGovId;
	}

	public ArrayList<NameValuePair> fillForm(Resources res) {
		this.mFormList = new ArrayList<NameValuePair>();
		CharSequence cs = DateFormat.format("yyyy-MM-dd:hh:mm", this.getDate());
		this.put(SmogMobileAPI.CarFind.TIME, cs);
		//
		this.put(SmogMobileAPI.CarFind.ADDR_STATE,
				FromState.toStateCode(res, this.mStateIndex));
		this.put(SmogMobileAPI.CarFind.ADDR_DISTRICT,
				FromState.toDistrictCode(this.mStateIndex, this.mDistrictIndex));
		this.put(SmogMobileAPI.CarFind.ADDR, this.getShortAddress());
		//
		this.put(SmogMobileAPI.CarFind.PLATE1, this.getLicensePlate1());
		this.put(SmogMobileAPI.CarFind.PLATE2, this.getLicensePlate2());
		this.put(SmogMobileAPI.CarFind.TYPE,
				res.getStringArray(R.array.car_type_value)[this.mCarTypeIndex]);
		return this.mFormList;
	}

	private void put(String key, Object value) {
		this.mFormList.add(new BasicNameValuePair(key, value.toString()));
	}

	private ArrayList<NameValuePair> mFormList;
}
