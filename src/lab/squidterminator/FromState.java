package lab.squidterminator;

import java.text.DecimalFormat;

import android.content.res.Resources;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Listener that set on the StateSpinner, it will automatically response the ItemClick and
 * set the DistrictSpinner(pass-in in the constructor) to the corresponding values.!!
 * @author TomChen
 *
 */
public class FromState implements OnItemSelectedListener {

	public static void initSelection(Spinner stateSpinner, int stateIdx,
			final Spinner districtSpinner, final int districtIdx) {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				stateSpinner.getContext(), R.array.state_names,
				R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		stateSpinner.setAdapter(adapter);
		stateSpinner.setSelection(stateIdx);
		stateSpinner.setOnItemSelectedListener(new FromState(districtSpinner));
		post(districtSpinner, districtIdx);
	}

	private static void post(final Spinner spinner, final int index) {
		/**
		 * Use async because of task queue
		 */
		spinner.postDelayed(new Runnable() {
			public void run() {
				spinner.setSelection(index);
			}
		}, 200L);
	}

	private final Spinner mDistrictSpinner;

	private FromState(Spinner districtSpinner) {
		super();
		this.mDistrictSpinner = districtSpinner;
	}

	public void onItemSelected(AdapterView<?> arg0, View notUsed, int stateIdx,
			long arg3) {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				arg0.getContext(), toDistrictNames(stateIdx),
				R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.mDistrictSpinner.setAdapter(adapter);
	}

	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public static String toStateCode(Resources res, int stateIdx) {
		return res.getStringArray(R.array.state_codes)[stateIdx];
	}

	public static int toDistrictNames(int stateIdx) {
		return sNamesMap[stateIdx];
	}

	public static String toDistrictCode(int stateIdx, int districtIdx) {
		switch (stateIdx) {
		case 4:
			if (districtIdx == 29)
				++districtIdx;
			break;
		case 17:
			if (districtIdx == 2)
				++districtIdx;
			else if (districtIdx > 2)
				districtIdx += 2;
			break;
		case 18:
			if (districtIdx > 16)
				++districtIdx;
			break;
		default:
			break;
		}
		++districtIdx;// array index 0-based, but code is start from 1;
		return new DecimalFormat("000 ").format(districtIdx);
	}

	private static final int[] sNamesMap = new int[] {
			R.array.district_names01, R.array.district_names02,
			R.array.district_names03, R.array.district_names04,
			R.array.district_names05, R.array.district_names06,
			R.array.district_names07, R.array.district_names08,
			R.array.district_names09, R.array.district_names10,
			R.array.district_names12, R.array.district_names13,
			R.array.district_names14, R.array.district_names15,
			R.array.district_names16, R.array.district_names17,
			R.array.district_names20, R.array.district_names21,
			R.array.district_names22, R.array.district_names23,
			R.array.district_names32, R.array.district_names33 };
}
